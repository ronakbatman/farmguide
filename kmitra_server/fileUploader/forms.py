from django.forms import ModelForm
from django import forms

from .models import District, Panchayat, Village
from .helpers import clean_non_ascii
import csv
import json


class BaseImportMixin(ModelForm):
    file_to_import = forms.FileField(label="Select a file", help_text="max. 42 megabytes")
    limiter = ','

    def save(self, commit=False, *args, **kwargs):
        """
        TODO:
        DO basic processing of form here
        """
        if self.model_name is None:
            return None

    def process_form(self,**kwargs):
        """
        define a logic to process a form
        TODO
        """


class DistrictDataImport(BaseImportMixin):
    district_name = forms.CharField(label='Enter District', help_text='Enter name of district for which you have to enter data')
    limiter = ','
    
    class Meta:
        model = District
        fields = ("district_name", "file_to_import",)

    def save(self, commit=False, *args, **kwargs):
        district_name = self.cleaned_data['district_name']
        file_csv = self.cleaned_data['file_to_import']
        records = csv.reader(file_csv)
        # print district_name
        p,created = District.objects.get_or_create(district_name=str(district_name))
        header = next(records)
        # print(header)
        data={}
        main_json = {}
        panchayat_dict = {}
        panchayat_list = []
        for line in records:
            sr_no = clean_non_ascii(line[0])
            panchayat = clean_non_ascii(line[1])
            village = clean_non_ascii(line[2])
            if panchayat in data:
                data[panchayat].append((sr_no,village))
            else:
                data[panchayat] = [(sr_no,village)]

        for keys in data.keys():
            keys = clean_non_ascii(keys)
            pp,created = Panchayat.objects.get_or_create(district=p,panchayat_name=str(keys))
            panchayat_dict[keys] = pp

        for keys in data.keys():
            keys = clean_non_ascii(keys)
            panchayat_json = {}
            panchayat_json["panchayat"] = keys
            panchayat_villages = []
            for village in data[keys]:
                v_id = clean_non_ascii(village[0])
                v_name =  clean_non_ascii(village[1])
                panchayat_village = {}
                panchayat_village["id"] = v_id
                panchayat_village["name"] = v_name
                panchayat_villages.append(panchayat_village)
                panchayat_fk = panchayat_dict[keys]
                Village.objects.get_or_create(village_id=v_id, village_name=v_name, 
                                            village_panchayat=panchayat_fk)

            panchayat_json["villages"] = panchayat_villages
            panchayat_list.append(panchayat_json)

        main_json["district"] = district_name
        main_json["panchayats"] = panchayat_list
        return main_json    