# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fileUploader', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='District',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('district_name', models.CharField(max_length=50)),
            ],
            options={
                'verbose_name_plural': 'Districts',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Panchayat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('panchayat_name', models.CharField(max_length=50)),
                ('district', models.ForeignKey(to='fileUploader.District')),
            ],
            options={
                'verbose_name_plural': 'District Panchayats',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Village',
            fields=[
                ('village_id', models.IntegerField(serialize=False, primary_key=True)),
                ('village_name', models.CharField(max_length=50)),
                ('village_panchayat', models.ForeignKey(to='fileUploader.Panchayat')),
            ],
            options={
                'verbose_name_plural': 'Villages',
            },
            bases=(models.Model,),
        ),
    ]
