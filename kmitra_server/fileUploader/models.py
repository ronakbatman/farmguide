from django.db import models
from django.contrib.auth.models import UserManager



# Create your models here.
class District(models.Model):

    district_name = models.CharField(max_length=50)

    def __unicode__(self):
        return '%s' % self.district_name

    class Meta:
        verbose_name_plural = "Districts"

    # objects = UserManager


class Panchayat(models.Model):
    district = models.ForeignKey('District')
    panchayat_name = models.CharField(max_length=50)

    class Meta:
        verbose_name_plural = "District Panchayats"


class Village(models.Model):
    village_id = models.IntegerField(primary_key=True)
    village_name = models.CharField(max_length=50)
    village_panchayat = models.ForeignKey('Panchayat')


    class Meta:
        verbose_name_plural = "Villages"

    