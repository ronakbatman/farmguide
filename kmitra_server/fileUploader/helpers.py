"""
Helpers methods
"""


def clean_non_ascii(value):
	"""
	clean non ascii values 
	"""
	return ''.join([i if ord(i) < 128 else '' for i in value])