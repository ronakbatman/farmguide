from django.http import HttpResponse

from django.views import generic
import json

from .forms import DistrictDataImport



class DistrictView(generic.TemplateView):
    """ About country import view. """
    template_name = 'fileUploader/import_form.html'

    def post(self, request, *args, **kwargs):
        print 'in post request'
        form = DistrictDataImport(request.POST, request.FILES)
        if form.is_valid():
            district_json = form.save()
            success = True
            return HttpResponse(json.dumps(district_json), content_type='application/json')

    def get_context_data(self, **kwargs):
        form = DistrictDataImport()
        ctx = super(DistrictView, self).get_context_data(**kwargs)
        ctx['form'] = form  # add something to ctx
        return ctx