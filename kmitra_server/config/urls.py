from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework import routers, serializers, viewsets
from django.contrib.auth.models import User
from django.views.generic import TemplateView
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from django.conf.urls.static import static

from authentication.views import RestrictedView, VolunteerRegistration, DisplayAllVolunteer, DisplayVolunteer

from fileUploader.views import DistrictView

# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'is_staff')

# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'users', UserViewSet)


urlpatterns = patterns('',

    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^get_auth_token/', 'rest_framework_jwt.views.obtain_jwt_token'),
    url(r'^add_farmer/$', RestrictedView.as_view()),
    url(r'^district/$',DistrictView.as_view()),
    url(r'^add_volunteer/$',VolunteerRegistration.as_view()),
    url(r'^thanks/', TemplateView.as_view(template_name="authentications/thanks.html")),
    url(r'^all_volunteers/$',DisplayAllVolunteer.as_view()),
    url(r'^volunteer/(?P<pk>\d+)/$', DisplayVolunteer.as_view()),
)

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
