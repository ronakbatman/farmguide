from django.shortcuts import render

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
 
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from django.contrib.auth.models import User
from django.views.generic.edit import FormView
from django.views.generic.list import ListView
from django.views.generic import DetailView
from rest_framework import generics
from authentication.serializers import UserSerializer,VolunteerSerializer

from authentication.models import Volunteer
from authentication.forms import VolunteerForm

 
 
class RestrictedView(APIView):
    """
    A authenticated user can save farmer data.
    Allowed Methods: POST and GET
    Test Url: /api/v1/add_farmer
    """

    permission_classes = (IsAuthenticated, )
    authentication_classes = (JSONWebTokenAuthentication, )
 
    def get(self, request,*args, **kwargs):
        """
        This method handles get request for the above api
        """
        data = {
            'id': request.user.id,
            'username' : request.user.username,
            'email': request.user.email
        }
        return Response(str(request.user))



    def post(self,request,*args, **kwargs):
        
        first_name = request.POST.get('first_name').strip()
        mobile_number = request.POST.get('mobile_number').strip()

        if not (first_name and mobile_number):
            return self.error("First name and mobile_number cannot be null")

    	user_id = request.user.id 



class VolunteerRegistration(FormView):
    template_name = 'authentications/volunteer_registration.html'
    form_class = VolunteerForm
    success_url = '/thanks/'

    def get_context_data(self, **kwargs):
        context = super(VolunteerRegistration, self).get_context_data(**kwargs)
        context["testing_out"] = "this is a new context var"
        return context

    def form_valid(self, form):
        form.save(commit=True)
        return super(VolunteerRegistration, self).form_valid(form)

class DisplayAllVolunteer(ListView):
    model = Volunteer
    queryset = Volunteer.objects.order_by('-date_joined')
    template_name = "authentications/display_all_volunteer.html"
    context_object_name = "volunteer_list"


class DisplayVolunteer(DetailView):
    model = Volunteer
    template_name='authentications/display_volunteer.html'

    def get_context_data(self, **kwargs):
         context = super(DisplayVolunteer, self).get_context_data(**kwargs)
         #Add some extra for view
         return context


class UserListAPIView(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class VolunteerListAPIView(generics.ListAPIView):
    queryset = Volunteer.objects.all()
    serializer_class = VolunteerSerializer

