# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import authentication.models


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
        ('authentication', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Crop',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('crop', models.CharField(default=b'N/A', max_length=50)),
                ('crop_type', models.CharField(default=b'N/A', max_length=50)),
                ('crop_season', models.CharField(default=b'N/A', max_length=30)),
                ('quantity', models.IntegerField(help_text=b'Quantity of crop')),
            ],
            options={
                'verbose_name_plural': 'Crops',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Farmer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(default=b'', max_length=50)),
                ('last_name', models.CharField(default=b'', max_length=50, null=True, blank=True)),
                ('father_name', models.CharField(max_length=50, null=True, blank=True)),
                ('mobile_number', models.CharField(max_length=15, null=True, blank=True)),
                ('url', models.CharField(max_length=255, null=True, blank=True)),
                ('profile_image', models.ImageField(null=True, upload_to=authentication.models.get_image_path, blank=True)),
                ('family_size', models.IntegerField(help_text=b'Family size')),
                ('email', models.CharField(max_length=50, null=True, blank=True)),
            ],
            options={
                'verbose_name_plural': 'Farmers',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Field',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('field_size', models.CharField(max_length=30)),
                ('normalized_field_size', models.DecimalField(default=0, max_digits=9, decimal_places=4)),
                ('irrigation_facility', models.CharField(default=b'Ground Water', max_length=50)),
                ('seed_procurement', models.CharField(max_length=50, null=True, blank=True)),
                ('market_aceess', models.CharField(max_length=40, null=True, blank=True)),
                ('soil_testing', models.BooleanField(default=False, help_text=b"Determines whether this field's soil is tested", verbose_name=b'Seed Testing')),
                ('gps_coordinate', models.CharField(max_length=50, null=True, blank=True)),
                ('owned_by', models.ForeignKey(to='authentication.Farmer')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Volunteer',
            fields=[
                ('user_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('mobile_number', models.CharField(max_length=15)),
            ],
            options={
                'verbose_name_plural': 'Volunteers',
            },
            bases=('auth.user',),
        ),
        migrations.AddField(
            model_name='farmer',
            name='registered_by',
            field=models.ForeignKey(to='authentication.Volunteer'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='crop',
            name='grown_by',
            field=models.ForeignKey(to='authentication.Farmer'),
            preserve_default=True,
        ),
    ]
