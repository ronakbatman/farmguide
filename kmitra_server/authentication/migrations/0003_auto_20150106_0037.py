# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators
import authentication.models


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0002_auto_20141229_2154'),
    ]

    operations = [
        migrations.AddField(
            model_name='volunteer',
            name='address',
            field=models.TextField(blank=True, null=True, validators=[django.core.validators.MaxLengthValidator(200)]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='volunteer',
            name='age',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='volunteer',
            name='date_of_birth',
            field=models.DateField(max_length=8, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='volunteer',
            name='father_name',
            field=models.CharField(max_length=30, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='volunteer',
            name='profile_image',
            field=models.ImageField(null=True, upload_to=authentication.models.get_image_path, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='volunteer',
            name='sex',
            field=models.CharField(default='Male', max_length=15, choices=[(b'Male', b'Male'), (b'Female', b'Female'), (b'Others', b'Others')]),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='volunteer',
            name='mobile_number',
            field=models.CharField(max_length=15, null=True, blank=True),
            preserve_default=True,
        ),
    ]
