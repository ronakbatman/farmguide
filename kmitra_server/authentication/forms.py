from django import forms
from django.contrib import admin
from django.contrib.auth.hashers import is_password_usable
from django.core.files.images import get_image_dimensions
from django.forms import extras
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User


from authentication.models import Farmer,Volunteer, Crop, Field

class JQueryDatePickerWidget(forms.DateInput):
    def __init__(self, **kwargs):
        super(forms.DateInput, self).__init__(attrs={"size":10, "class": "dateinput"}, **kwargs)

    class Media:
        css = {"all":("http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/themes/redmond/jquery-ui.css",)}
        js = ("http://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js",
              "http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/jquery-ui.min.js",)


class FarmerForm(forms.ModelForm):
	mobile_number =  forms.CharField(required=False)
	url = forms.CharField(required=False)
	father_name = forms.CharField(required=False)

	class Meta:
		model = Farmer
		fields = ('first_name','last_name','father_name','mobile_number','url','profile_image','registered_by','family_size')



class VolunteerForm(forms.ModelForm):
	password = forms.CharField(widget=forms.PasswordInput(render_value=True))
	date_of_birth = forms.DateField(label=_("Date of birth"), widget=JQueryDatePickerWidget)
	address = forms.CharField(widget = forms.Textarea)
	mobile_number =  forms.CharField(widget=forms.TextInput(attrs={'max_length':1}),
        label=_("Phone/Mobile Number"))

	class Meta:
		model = Volunteer
		fields = ('username','password','first_name','last_name','age','sex',
			'father_name','mobile_number','date_of_birth','address','profile_image',)

	def clean_profile_image(self):
		profile_image = self.cleaned_data['profile_image']
		try:
			w, h = get_image_dimensions(profile_image)
            #validate dimensions
			max_width = max_height = 200
			if w > max_width or h > max_height:
				raise forms.ValidationError(
					u'Please use an image that is %s x %s pixels or smaller.' % (max_width, max_height))

			main, sub = profile_image.content_type.split('/')
			if not (main == 'image' and sub in ['jpeg', 'pjpeg', 'gif', 'png']):
				raise forms.ValidationError(u'Please use a JPEG, GIF or PNG image.')

            #validate file size
			if len(profile_image) > (20 * 1024):
				raise forms.ValidationError('Avatar file size may not exceed 20KB.')
			
		except:
			pass

		return profile_image

	def clean_username(self):
		username = self.cleaned_data['username']
		if User.objects.exclude(pk=self.instance.pk).filter(username=username).exists():
			raise forms.ValidationError(u'Username "%s" is already in use.' % username)
		return username

	def save(self, *args, **kwargs):
		"""
		Override save method to set user password.
		"""
		instance = super(VolunteerForm, self).save(*args, **kwargs)
		if not is_password_usable(instance.password):
			instance.set_password(instance.password)
		return instance

class CropForm(forms.ModelForm):
	class Meta:
		model = Crop
		exclude = ()

class FieldForm(forms.ModelForm):

	class Meta:
		
		model = Field
		exclude = ('normalized_field_size',)