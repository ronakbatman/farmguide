from django.db import models
from django.contrib.auth.models import User, UserManager
from django.core.validators import MaxLengthValidator


import os

SEX=(('Male','Male'),
    ('Female','Female'),
    ('Others','Others'),)


def get_image_path(instance, filename):
	return os.path.join('images', str(instance.id), filename)



class Volunteer(User):
	age = models.IntegerField(default=0)
	sex = models.CharField(choices=SEX,max_length=15)
	mobile_number = models.CharField(max_length = 15,null=True,blank=True)
	father_name = models.CharField(max_length = 30,null=True,blank=True)
	date_of_birth = models.DateField(max_length=8,null=True,blank=True)
	address = models.TextField(validators=[MaxLengthValidator(200)],null=True,blank=True)
	profile_image = models.ImageField(upload_to='', blank=True, null=True)
	class Meta:
		verbose_name_plural = "Volunteers"

	def __unicode__(self):
		return "%s" % self.first_name+" "+self.last_name

	def __str__(self):
		return "%s" % self.first_name+" "+self.last_name



class Farmer(models.Model):
	
	first_name = models.CharField(max_length=50,default="")
	last_name = models.CharField(max_length=50,blank=True, null=True,default="")
	father_name = models.CharField(max_length=50, blank=True, null=True)
	mobile_number = models.CharField(max_length = 15,blank=True, null=True)
	url = models.CharField(max_length=255,blank=True, null=True)
	profile_image = models.ImageField(upload_to='', blank=True, null=True)
	registered_by = models.ForeignKey(Volunteer)
	family_size =  models.IntegerField(help_text="Family size")
	email = models.CharField(max_length=50,blank=True, null=True)

	def cache(self):
		"""Store image locally if we have a URL"""
		if self.url and not self.profile_image:
			result = urllib.urlretrieve(self.url)
			self.photo.save(
					os.path.basename(self.url),
					File(open(result[0]))
					)
			self.save()

	class Meta:
		verbose_name_plural = "Farmers"

	def __unicode__(self):
		return "%s" % self.first_name

	def __str__(self):
		return "%s" % self.first_name+" "+self.last_name


class Crop(models.Model):

	"""This class stores information related crops sown by farmer"""

	crop = models.CharField(max_length =50, default = "N/A")
	crop_type = models.CharField(max_length =50, default = "N/A")
	crop_season = models.CharField(max_length = 30, default= "N/A")
	quantity = models.IntegerField(help_text = "Quantity of crop")
	grown_by = models.ForeignKey(Farmer)

	class Meta:
		verbose_name_plural = "Crops"

	def __unicode__(self):
		return "%s" % self.crop

	def __str__(self):
		return "Crop = %s Quantity = %s" % (self.crop+" "+self.type, quantity)


class Field(models.Model):
	"""
	This class stores field related information of a farmer
	"""
	field_size = models.CharField(max_length = 30)
	normalized_field_size = models.DecimalField(default=0, max_digits=9, decimal_places=4)
	irrigation_facility = models.CharField(max_length=50, default = "Ground Water")
	seed_procurement = models.CharField(max_length=50,null=True, blank=True)
	market_aceess = models.CharField(max_length =40,null=True, blank=True)
	soil_testing = models.BooleanField(('Seed Testing'), default=False,
        help_text=('Determines whether this field\'s soil is tested'))
	gps_coordinate = models.CharField(max_length =50, null=True, blank=True)
	owned_by = models.ForeignKey(Farmer)

	def __unicode__(self):
		return "%s" % (self.field_size)

	def __str__(self):
		return "Owner = %s " % (self.field_size)
