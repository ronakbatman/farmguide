from django.contrib import admin
from authentication.models import Farmer,Volunteer,Crop, Field
from forms import FarmerForm,VolunteerForm,CropForm, FieldForm

# Register your models here.

@admin.register(Farmer)
class FarmerAdmin(admin.ModelAdmin):
    form = FarmerForm
    list_display = ('first_name','last_name','father_name','mobile_number',)


@admin.register(Volunteer)
class VolunteerAdmin(admin.ModelAdmin):
    form = VolunteerForm
    list_display = ('first_name','last_name','mobile_number')


@admin.register(Crop)
class CropAdmin(admin.ModelAdmin):
    form = CropForm
    list_display = ('crop','crop_type','crop_season','quantity',)


@admin.register(Field)
class FieldAdmin(admin.ModelAdmin):
    form = FieldForm
    list_display = ('field_size','gps_coordinate')