from django.contrib.auth.models import User, Group

from rest_framework import serializers

from authentication.models import Volunteer


class UserSerializer(serializers.ModelSerializer):

	class Meta:
		model = User
		fields = ('id', 'username', 'email')

class VolunteerSerializer(serializers.ModelSerializer):

	class Meta:
		model = Volunteer
		fields = ('id', 'username', 'email')